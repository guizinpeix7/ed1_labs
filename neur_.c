/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nome: Guilherme Peixoto Lima
 * Comando compilação(Linux): gcc neur_.c
 * Programa que conta a quantidade de neuronios ativos e inativos.
*/

#include <stdio.h>
#define MAX 2

int fneuronio(int *entrada,int *peso,int *t ,int  max){
        int SOMAP = 0;

        for(int i = 0 ; i < max ; i++){
                (SOMAP) += (*(peso+i))*(*(peso+i));
        }

        if(SOMAP > *t)  return 1;
        else return 0;                                                                         }

int main(){

        int ENTRADA[MAX];// = {0,0,0,0,0,0,0,0,0,0};
        int PESOS[MAX]; //= {0,0,0,0,0,0,0,0,0,0};
        int *limiar_T;

        for(int i = 0 ; i < MAX ; i++){
                scanf("%d",(ENTRADA+i)); //Isso indica que o falor esta sendo armazenado dentro
 do endereco de onde o espaco de memoria aponta (ENTRADA+i) == &ENTRADA[i]
                //printf("%d\n", *(ENTRADA+i));
                scanf(" %d", (PESOS+i));
//printf("%d\n", *(PESOS+i));                                                          }
        scanf(" %d", limiar_T);
        int teste;
        teste = fneuronio(ENTRADA,PESOS,limiar_T,MAX);

        printf("%d\n",teste);

        return 0;
}
