/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nome: Guilherme Peixoto Lima
 * Comando compilação(Linux): gcc neur_.c
 * Programa que conta a quantidade de neuronios ativos e inativos.
*/

#include <stdio.h>
#define MAX 10

int fneuronio(int *entrada,int *peso,int *t ,int  max){
        int SOMAP = 0;

        for(int i = 0 ; i < max ; i++){
	       	(SOMAP) += (*(entrada+i))*(*(peso+i));
        } 
        if(SOMAP > *t)  return 1;
        else return 0;                                                                         
}

int main(){

        int ENTRADA[MAX];// = {0,0,0,0,0,0,0,0,0,0};
        int PESOS[MAX]; //= {0,0,0,0,0,0,0,0,0,0};
        int *limiar_T;

	printf("Bem vindo ao programa que determinara o estado de um neuronio logico\n...por favor siga os comandos a seguir\n\n");

        for(int i = 0 ; i < MAX ; i++){
	  printf("Insira a entrada %d\n", i);
          scanf("%d",(ENTRADA+i)); //Isso indica que o falor esta sendo armazenado dentro do endereco de onde o espaco de memoria aponta (ENTRADA+i) == &ENTRADA[i]
                //printf("%d\n", *(ENTRADA+i));   
        }

	for(int i = 0 ; i < MAX ; i++){
	   printf("Por favor insira o peso do neuronio %d\n", i); 	
	   scanf(" %d", (PESOS+i));

	}

        scanf(" %d", limiar_T);
        int teste;
        teste = fneuronio(ENTRADA,PESOS,limiar_T,MAX);
	
	puts("");
        printf("%d\n",teste);
	(teste) ? puts("Neuronio ativado!") :  puts("Neuronio inibido!"); 
	puts("");

        return 0;
}
