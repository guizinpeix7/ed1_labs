/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nome: Guilherme Peixoto Lima
 * Comando compilação(Linux): gcc neur_.c
 * Programa que informa a condicao de aprovacao dos alunos
*/

#include <stdio.h> 
#define MAX 10

///////////////////////////////////////CABECALHO////////////////////////////////////////////////////
int percent_aprov(double *,double *,double *,double *,int);
int conta_notas(int *, int ,double *,double*);
int recebe_notas(double * ,int *, int);

////////////////////////////////////////MAIN//////////////////////////////////////////////////////////
int main(){
    double notas[MAX];
    int APR[MAX];
    double percent_aprov_var = 0, percent_reprov_var= 0; 
    double *aprovadosPtr, *reprovadosPtr;        
    double *percent_aprovPtr,  *percent_reprovPtr;
    double aprovados = 0, reprovados = 0;
    int maioria; 

    puts("Bem vindo ao meu, ao seu, ao nosso programa que calcula a quantidade de alunos aprovados"); 
    puts("Por favor siga os passos a seguir...\n");

    percent_aprovPtr = &percent_aprov_var; 
    percent_reprovPtr = &percent_reprov_var; 	
    aprovadosPtr = &aprovados;
    reprovadosPtr = &reprovados;

    for(int i = 0 ; i < MAX ; i++){
	    printf("Insira a nota do aluno %d\n",i ); 
            scanf("%lf", (notas+i));
    }
    recebe_notas(notas,APR,MAX);
    conta_notas(APR, MAX, aprovadosPtr,reprovadosPtr);
    maioria = percent_aprov(aprovadosPtr,reprovadosPtr,percent_aprovPtr,percent_reprovPtr,MAX);	
	   
    puts("");
    printf("Quantidade de aprovados %.0lf\n" , *aprovadosPtr);
    printf("Quantidade de reprovados %.0lf\n", *reprovadosPtr);
    printf("O percentual de aprovados foi de: %.2lf\n", *percent_aprovPtr);
    printf("O percentual de reprovados foi de: %.2lf\n", *percent_reprovPtr); 
    if(maioria) puts("Mais da metade da turma aprovada");

    puts("");

    return 0;
}
//////////////////////////////////////FUNCAO QUE RETORNA A PORCENTAGEM  DE APROVADOS////////////////////////////////
int percent_aprov(double *aprovados, double *reprovados, double *percent_aprov, double *percent_reprov,int elementos){ 
    *percent_aprov = ((*aprovados)/(double) elementos) * 100;
    *percent_reprov = 100.0 - *percent_aprov;

    return (*percent_aprov >50.00) ? 1 : 0; 
}
//////////////////////////////////////FUNCAO QUE CALCULA O NUMERO DE APROVADOS E REPROVADOS////////////////////////
int conta_notas(int *APR, int elementos,double *aprovadosPtr,double *reprovadosPtr){
    
        for(int i = 0 ; i < elementos ; i++){
                if(*(APR+i) == 1) *(aprovadosPtr) +=1;
        }

        *reprovadosPtr = elementos - (*(aprovadosPtr));
        
	return 1;
}
/////////////////////////////////////FUNCAO QUE PREENCHE APR COM 1 E 0/////////////////////////////////////////////
int recebe_notas(double *nota,int *APR, int elementos){
 
 	for(int i = 0 ; i < elementos ; i++){
                if(*(nota+i) >= 6.0) *(APR+i) = 1;
                else *(APR+i) = 0;
        }
        return 1;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
