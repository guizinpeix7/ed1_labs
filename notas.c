/* Universidade de Brasília - Faculdade do Gama
 * Estruturas de Dados 1 - 193704 TURMA: B
 * Professor: Mateus Mendelson
 * Nome: Guilherme Peixoto Lima
 * Comando compilação(Linux): gcc neur_.c
 * Programa que informa a condicao de aprovacao dos alunos
*/

#include <stdio.h>
#define MAX 10

int percent_aprov(double,double,double,double,int);
int conta_notas(int , int ,double ,double);
int recebe_notas(double ,int, int);

int percent_aprov(double *aprovados, double *reprovados, double *percent_aprov, double *percent_reprov,int elementos){ 
	*percent_aprov = ((*aprovados)/elementos) * 100;
    *percent_reprov = 100.0 - *percent_aprov;
    
	return (*percent_aprov >50.00) ? 1 : 0; 
 }

int conta_notas(int *APR, int elementos,double *aprovadosPtr,double *reprovadosPtr){

        printf("%d\n", *aprovadosPtr);
        for(int i = 0 ; i < elementos ; i++){
                if(*(APR+i) == 1) *(aprovadosPtr) +=1;
        }

        *reprovadosPtr = elementos - (*(aprovadosPtr));
        
	return 1;
}

int recebe_notas(double *nota,int *APR, int elementos){
        for(int i = 0 ; i < elementos ; i++){
                if(*(nota+i) >= 6.0) *(APR+i) = 1;
                else *(APR+i) = 0;
        }
        return 1;
}

int main(){
    double notas[MAX];
    int APR[MAX];
    double percent_aprov_var = 0, percent_reprov_var= 0; 
    double *aprovadosPtr, *reprovadosPtr;        
	double *percent_aprovPtr,  *percent_reprovPtr;
 	double aprovados = 0, reprovados = 0;

        percent_aprovPtr = &percent_aprov_var; 
        percent_reprovPtr = &percent_reprov_var; 	

        aprovadosPtr = &aprovados;
        reprovadosPtr = &reprovados;

        for(int i = 0 ; i < MAX ; i++){
                scanf("%lf", (notas+i));
        }
        recebe_notas(notas,APR,MAX);

        for(int i = 0 ; i < MAX ; i++){
                printf("%d\n", *(APR+i));
        }

        conta_notas(APR, MAX, aprovadosPtr,reprovadosPtr);

	percent_aprov(aprovadosPtr,reprovadosPtr,percent_aprovPtr,percent_reprovPtr,MAX); 

        return 0;
}
